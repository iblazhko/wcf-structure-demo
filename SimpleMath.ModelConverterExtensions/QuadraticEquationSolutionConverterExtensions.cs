﻿using SimpleMath.DomainModel;
using SimpleMath.WcfContracts;
using System.Numerics;

namespace SimpleMath.ModelConverterExtensions
{
    public static class QuadraticEquationSolutionConverterExtensions
    {
        public static QuadraticEquationSolutionDto ToDto(this QuadraticEquationSolution domain)
        {
            return domain != null ?
                new QuadraticEquationSolutionDto
                {
                    SolutionKind = domain.SolutionKind.ToDto(),
                    X1 = new Complex(domain.X1.Real, domain.X1.Imaginary),
                    X2 = new Complex(domain.X2.Real, domain.X2.Imaginary)
                } : 
                null;
        }

        public static QuadraticEquationSolution ToDomain(this QuadraticEquationSolutionDto dto)
        {
            return dto != null ?
                new QuadraticEquationSolution(
                    dto.SolutionKind.ToDomain(),
                    new Complex(dto.X1.Real, dto.X1.Imaginary),
                    new Complex(dto.X2.Real, dto.X2.Imaginary)
                ):
                null;
        }
    }
}
