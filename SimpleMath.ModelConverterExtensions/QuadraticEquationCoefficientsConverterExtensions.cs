﻿using SimpleMath.DomainModel;
using SimpleMath.WcfContracts;

namespace SimpleMath.ModelConverterExtensions
{
    public static class QuadraticEquationCoefficientsConverterExtensions
    {
        public static QuadraticEquationCoefficientsDto ToDto(this QuadraticEquationCoefficients domain)
        {
            return domain != null ?
                new QuadraticEquationCoefficientsDto
                {
                    A = domain.A,
                    B = domain.B,
                    C = domain.C
                } : 
                null;
        }

        public static QuadraticEquationCoefficients ToDomain(this QuadraticEquationCoefficientsDto dto)
        {
            return dto != null ?
                new QuadraticEquationCoefficients(dto.A, dto.B, dto.C) :
                null;
        }
    }
}
