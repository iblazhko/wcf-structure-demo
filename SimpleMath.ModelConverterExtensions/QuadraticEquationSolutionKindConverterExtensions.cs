﻿using SimpleMath.DomainModel;
using SimpleMath.WcfContracts;
using System;

namespace SimpleMath.ModelConverterExtensions
{
    public static class QuadraticEquationSolutionKindConverterExtensions
    {
        public static QuadraticEquationSolutionKindDto ToDto(this QuadraticEquationSolutionKind domain)
        {
            return (QuadraticEquationSolutionKindDto)Enum.Parse(typeof(QuadraticEquationSolutionKindDto), domain.ToString(), true);
        }

        public static QuadraticEquationSolutionKind ToDomain(this QuadraticEquationSolutionKindDto dto)
        {
            return (QuadraticEquationSolutionKind)Enum.Parse(typeof(QuadraticEquationSolutionKind), dto.ToString(), true);
        }
    }
}
