﻿using System.Runtime.Serialization;

namespace SimpleMath.WcfContracts
{
    [DataContract(Name = "QuadraticEquationSolutionKind", Namespace = WcfContractsNamespace.Namespace)]
    public enum QuadraticEquationSolutionKindDto
    {
        [EnumMember]
        Trivial = 0,

        [EnumMember]
        Real = 1,

        [EnumMember]
        Complex = 2
    }
}
