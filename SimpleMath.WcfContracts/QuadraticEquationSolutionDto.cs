﻿using System.Numerics;
using System.Runtime.Serialization;

namespace SimpleMath.WcfContracts
{
    [DataContract(Name = "QuadraticEquationSolution", Namespace = WcfContractsNamespace.Namespace)]
    public class QuadraticEquationSolutionDto
    {
        [DataMember(Name = "SolutionKind", Order = 1)]
        public QuadraticEquationSolutionKindDto SolutionKind { get; set; }

        [DataMember(Order = 2)]
        public Complex X1 { get; set; }
        
        [DataMember(Order = 3)]
        public Complex X2 { get; set; }
    }
}
