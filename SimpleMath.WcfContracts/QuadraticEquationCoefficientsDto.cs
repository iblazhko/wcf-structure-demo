﻿using System.Runtime.Serialization;

namespace SimpleMath.WcfContracts
{
    [DataContract(Name = "QuadraticEquationCoefficients", Namespace = WcfContractsNamespace.Namespace)]
    public class QuadraticEquationCoefficientsDto
    {
        [DataMember(Order = 1)]
        public double A { get; set; }

        [DataMember(Order = 2)]
        public double B { get; set; }
        
        [DataMember(Order = 3)]
        public double C { get; set; }
    }
}
