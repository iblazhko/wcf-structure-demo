﻿using System.ServiceModel;

namespace SimpleMath.WcfContracts
{
    [ServiceContract(Name = "SolverService", Namespace = WcfContractsNamespace.Namespace)]
    public interface ISolverService
    {
        [OperationContract]
        QuadraticEquationSolutionDto Solve(QuadraticEquationCoefficientsDto coefficients);
    }
}
