﻿using SimpleMath.WcfContracts;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace SimpleMath.WcfProxies
{
    public class SolverServiceProxy : ClientBase<ISolverService>, ISolverService
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the SolverClient class.
        /// </summary>
        public SolverServiceProxy()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SolverClient class.
        /// </summary>
        /// <param name="endpointConfigurationName">Configuration name in app.config/web.config</param>
        public SolverServiceProxy(string endpointConfigurationName)
            : base(endpointConfigurationName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the SolverClient class.
        /// </summary>
        /// <param name="endpointConfigurationName">Configuration name in app.config/web.config</param>
        /// <param name="remoteAddress">Remote service address.</param>
        public SolverServiceProxy(string endpointConfigurationName, EndpointAddress remoteAddress)
            : base(endpointConfigurationName, remoteAddress)
        {
        }

        /// <summary>
        /// Initializes a new instance of the SolverClient class.
        /// </summary>
        /// <param name="inputInstance">Object providing client context information.</param>
        public SolverServiceProxy(InstanceContext inputInstance)
            : base(inputInstance)
        {
        }

        /// <summary>
        /// Initializes a new instance of the SolverClient class.
        /// </summary>
        /// <param name="inputInstance">Object providing client context information.</param>
        /// <param name="endpointConfigurationName">Configuration name in app.config/web.config</param>
        public SolverServiceProxy(InstanceContext inputInstance, string endpointConfigurationName)
            : base(inputInstance, endpointConfigurationName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the SolverClient class.
        /// </summary>
        /// <param name="binding">Endpoint binding configuration.</param>
        /// <param name="remoteAddress">emote service address.</param>
        public SolverServiceProxy(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region SolverClient Members

        public QuadraticEquationSolutionDto Solve(QuadraticEquationCoefficientsDto coefficients)
        {
            return Channel.Solve(coefficients);
        }

        #endregion
    }
}
