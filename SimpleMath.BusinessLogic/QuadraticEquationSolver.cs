﻿using SimpleMath.DomainModel;
using System;
using System.Numerics;

namespace SimpleMath.BusinessLogic
{
    public class QuadraticEquationSolver: IQuadraticEquationSolver
    {
        public QuadraticEquationSolution Solve(QuadraticEquationCoefficients coefficients)
        {
            if (coefficients == null)
            {
                throw new ArgumentNullException("coefficients");
            }
            
            var a = coefficients.A;
            var b = coefficients.B;
            var c = coefficients.C;

            QuadraticEquationSolution result;

            double sqrtPart = (b * b) - (4 * a * c);

            if (sqrtPart > 0)
            {
                result = new QuadraticEquationSolution(
                    QuadraticEquationSolutionKind.Real,
                    new Complex((-b + Math.Sqrt(sqrtPart)) / (2 * a), 0),
                    new Complex((-b - Math.Sqrt(sqrtPart)) / (2 * a), 0));
            }
            else if (sqrtPart < 0)
            {
                var real = -b / (2 * a);
                var imaginary = Math.Sqrt(-sqrtPart) / (2 * a);

                result = new QuadraticEquationSolution(
                    QuadraticEquationSolutionKind.Complex,
                    new Complex(real, imaginary),
                    new Complex(real, -imaginary));
            }
            else // sqrtPart == 0
            {
                var x = -b / (2 * a);
                result = new QuadraticEquationSolution(
                    QuadraticEquationSolutionKind.Trivial,
                    x,
                    x);
            }

            return result;
        }
    }
}
