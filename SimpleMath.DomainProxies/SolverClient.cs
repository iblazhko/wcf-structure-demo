﻿using SimpleMath.DomainModel;
using SimpleMath.ModelConverterExtensions;
using SimpleMath.WcfContracts;

namespace SimpleMath.DomainProxies
{
    public class SolverClient : IQuadraticEquationSolver
    {
        private readonly ISolverService proxy;

        public SolverClient(ISolverService proxy)
        {
            this.proxy = proxy;
        }

        public QuadraticEquationSolution Solve(QuadraticEquationCoefficients coefficients)
        {
            return this.proxy.Solve(coefficients.ToDto()).ToDomain();
        }
    }
}
