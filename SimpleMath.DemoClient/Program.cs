﻿using SimpleMath.DomainModel;
using System;

namespace SimpleMath.DemoClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var client = CompositionRoot.ComposeClient();

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Enter coefficients [A,B,C]:");
                var input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    break;
                }

                var coefficientsAsString = input.Split(',');

                if ((coefficientsAsString != null) && (coefficientsAsString.Length == 3))
                {
                    double a, b, c;

                    if (double.TryParse(coefficientsAsString[0], out a) &&
                        double.TryParse(coefficientsAsString[1], out b) &&
                        double.TryParse(coefficientsAsString[2], out c))
                    {
                        QuadraticEquationCoefficients coefficients;
                        try
                        {
                            coefficients = new QuadraticEquationCoefficients(a, b, c);
                        }
                        catch (ArgumentException aex)
                        {
                            Console.WriteLine(aex.Message);
                            continue;
                        }

                        try
                        {
                            var solution = client.Solve(coefficients);
                            Console.WriteLine("Solution: " + solution);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Could not parse input");
                    }
                }
            }
        }
    }
}
