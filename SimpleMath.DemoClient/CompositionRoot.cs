﻿using SimpleMath.DomainProxies;
using SimpleMath.WcfProxies;

namespace SimpleMath.DemoClient
{
    internal static class CompositionRoot
    {
        public static SolverClient ComposeClient()
        {
            var proxy = new SolverServiceProxy();
            return new SolverClient(proxy);
        }
    }
}
