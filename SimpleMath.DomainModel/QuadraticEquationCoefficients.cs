﻿using System;

namespace SimpleMath.DomainModel
{
    public class QuadraticEquationCoefficients
    {
        public double A { get; private set; }
        public double B { get; private set; }
        public double C { get; private set; }

        public QuadraticEquationCoefficients(double a, double b, double c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All zeroes coefficients are not allowed");
            }

            this.A = a;
            this.B = b;
            this.C = c;
        }
    }
}
