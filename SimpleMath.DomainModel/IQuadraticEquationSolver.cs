﻿namespace SimpleMath.DomainModel
{
    public interface IQuadraticEquationSolver
    {
        QuadraticEquationSolution Solve(QuadraticEquationCoefficients coefficients);
    }
}
