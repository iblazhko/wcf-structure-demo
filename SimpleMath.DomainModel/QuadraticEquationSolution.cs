﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Numerics;

namespace SimpleMath.DomainModel
{
    public class QuadraticEquationSolution
    {
        public QuadraticEquationSolutionKind SolutionKind { get; private set; }
        public Complex X1 { get; private set; }
        public Complex X2 { get; private set; }

        public QuadraticEquationSolution(QuadraticEquationSolutionKind solutionKind, Complex x1, Complex x2)
        {
            if (!ValidateInvariant(solutionKind, x1, x2))
            {
                throw new ArgumentException("Invalid solution configuration");
            }

            this.SolutionKind = solutionKind;
            this.X1 = x1;
            this.X2 = x2;
        }


        public override string ToString()
        {
            switch (SolutionKind)
            {
                case QuadraticEquationSolutionKind.Trivial:
                    return string.Format(
                        CultureInfo.InvariantCulture,
                        "Kind={0};X={1}",
                        SolutionKind,
                        X1.Real);
                
                case QuadraticEquationSolutionKind.Real:
                    return string.Format(
                        CultureInfo.InvariantCulture,
                        "Kind={0};X1={1};X2={2}",
                        SolutionKind,
                        X1.Real,
                        X2.Real);
                
                case QuadraticEquationSolutionKind.Complex:
                    return string.Format(
                        CultureInfo.InvariantCulture,
                        "Kind={0};X1={1};X2={2}",
                        SolutionKind,
                        X1,
                        X2);

                default:
                    return base.ToString();
            }
        }

        private static bool ValidateInvariant(QuadraticEquationSolutionKind solutionKind, Complex x1, Complex x2)
        {
            switch (solutionKind)
            {
                case QuadraticEquationSolutionKind.Trivial:
                    return ((x1.Real == x2.Real) &&
                        (x1.Imaginary == 0) &&
                        (x2.Imaginary == 0));

                case QuadraticEquationSolutionKind.Real:
                    return ((x1.Real != x2.Real) &&
                        (x1.Imaginary == 0) &&
                        (x2.Imaginary == 0));

                case QuadraticEquationSolutionKind.Complex:
                    return ((x1.Imaginary != 0) &&
                        (x2.Imaginary != 0));

                default:
                    throw new InvalidOperationException("Solution kind " + solutionKind.ToString() + " is not supported");
            }
        }
    }
}
