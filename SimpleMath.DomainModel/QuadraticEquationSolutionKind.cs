﻿namespace SimpleMath.DomainModel
{
    public enum QuadraticEquationSolutionKind
    {
        /// <summary>
        /// One real number.
        /// </summary>
        Trivial,

        /// <summary>
        /// Two real numbers.
        /// </summary>
        Real,

        /// <summary>
        /// Two complex numbers.
        /// </summary>
        Complex
    }
}
