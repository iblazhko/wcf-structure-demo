﻿using SimpleMath.DomainModel;
using SimpleMath.ModelConverterExtensions;
using SimpleMath.WcfContracts;
using System;
using System.ServiceModel;

namespace SimpleMath.WcfServices
{
    [ServiceBehavior]
    public class SolverService: ISolverService
    {
        private readonly IQuadraticEquationSolver solver;

        public SolverService(IQuadraticEquationSolver solver)
        {
            if (solver == null)
            {
                throw new ArgumentNullException("solver");
            }

            this.solver = solver;
        }

        public QuadraticEquationSolutionDto Solve(QuadraticEquationCoefficientsDto coefficients)
        {
            return solver.Solve(coefficients.ToDomain()).ToDto();
        }
    }
}
