﻿namespace SimpleMath.SolverServiceHost
{
    internal static class CompositionRoot
    {
        public static WcfContracts.ISolverService ComposeSolverService()
        {
            return new WcfServices.SolverService(
               new BusinessLogic.QuadraticEquationSolver());
        }
    }
}
