﻿using SimpleMath.WcfContracts;
using System;
using System.ServiceModel;

namespace SimpleMath.SolverServiceHost
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ISolverService service = CompositionRoot.ComposeSolverService();

            using (var host = new ServiceHost(service))
            {
                var behaviour = host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
                behaviour.InstanceContextMode = InstanceContextMode.Single;

                host.Open();

                Console.WriteLine("SolverService is ready at {0}", host.BaseAddresses[0].ToString());
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
